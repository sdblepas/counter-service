FROM python:3.7-alpine3.14
COPY . /bin
WORKDIR /bin
RUN pip install --upgrade pip
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
EXPOSE 80
CMD ["counter-service.py"]

